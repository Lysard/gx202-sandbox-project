using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[CreateAssetMenu(fileName = "SO_Tool", menuName = "Assets/SO_Items", order = 0)]
public class SO_Tool : ScriptableObject {

    public  enum ToolType
    {
        WateringCan,
        Cutter
    }

    public ToolType toolType;

    public Sprite toolSprite;
    
}


