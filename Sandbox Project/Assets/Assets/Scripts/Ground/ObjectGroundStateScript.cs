using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectGroundStateScript : MonoBehaviour
{
    //Different ground states have different outcomes. The ground object is set to one of them.
    public enum ObjectGroundState {
        OnDirt,
        OnGrass,
        OnNothing
    }
    public ObjectGroundState objectGroundState = ObjectGroundState.OnNothing;
    private GroundScript groundScript;
    

    //On object hitting ground it accesses the ground's script and checks what state it is in - then it sets object's groundstate to /ondirt/ongrass/etc.
    private void OnTriggerEnter2D (Collider2D other) {
        if (other.GetComponent<GroundScript> () != null) {
            groundScript = other.GetComponent<GroundScript> ();
            switch (groundScript.groundType) {
                case GroundScript.GroundType.Dirt:
                   objectGroundState = ObjectGroundState.OnDirt;
                    Debug.Log ($"{gameObject} collider triggered | ObjectGroundState: {objectGroundState}");
                    break;
                case GroundScript.GroundType.Grass:
                    objectGroundState = ObjectGroundState.OnGrass;
                    Debug.Log ($"{gameObject} collider triggered | ObjectGroundState: {objectGroundState}");
                    break;
                default:

                    break;
            }

        }
    }
    //when you are not on the ground anymore - ground state is "OnNothing"
    private void OnTriggerExit2D (Collider2D other) {
        if (other.GetComponent<GroundScript> () != null) {
            groundScript = other.GetComponent<GroundScript> ();

            objectGroundState = ObjectGroundState.OnNothing;
            Debug.Log ($"{gameObject} collider triggered | ObjectGroundState: {objectGroundState}");
        }
    }
}
