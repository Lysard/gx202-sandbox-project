using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum ObjectType {
    Fruit,
    Seed,
    Tool

}

public class GroundLifeTimeScript : MonoBehaviour {
    public enum LifeState {
        Aging,
        NotAging,
        ReachedMaxLifetime
    }
    public LifeState lifeState = LifeState.NotAging;
    public ObjectType objectType;
    [SerializeField] float lifeLifeTime;
    [SerializeField] float time;
    private ObjectGroundStateScript objectGroundStateScript;

    public delegate void SpawnActions (Transform parentTransform, Transform poolTransform, GameObject spawnObject, GameObject spawnerObject);

    public static SpawnActions spawnEvent;

    //check if I am a fruit or seed
    private void Start () {
        objectGroundStateScript = gameObject.GetComponent<ObjectGroundStateScript> ();
    }
    private void FixedUpdate () {
        OnLifeTime ();

    }

    //while object is on ground, increase life untill reaching max lifetime. On reaching maxLifetime - objectpooling and stop aging

    private void OnLifeTime () {
        if (objectGroundStateScript.objectGroundState == ObjectGroundStateScript.ObjectGroundState.OnDirt && lifeState != LifeState.ReachedMaxLifetime) {

            switch (objectType) {
                case ObjectType.Seed:
                    if (lifeLifeTime < SeedManagerScript.Instance.OnSendMaxLifeFloat ()) {
                        if (lifeState == LifeState.NotAging) {
                            OnAging ();
                        }
                    } else {
                        OnMaxAge ();
                    }
                    break;
                case ObjectType.Fruit:
                    if (lifeLifeTime < FruitManagerScript.Instance.OnSendMaxLifeFloat ()) {
                        if (lifeState == LifeState.NotAging) {
                            OnAging ();
                        }
                    } else {
                        OnMaxAge ();
                    }
                    break;
                default:
                    break;
            }

        }
    }
    //if our lifetime is less than max lifetime - increase age
    private void OnAging () {
        lifeState = LifeState.Aging;
        StartCoroutine (OnlifeLifetimeIncrease ());
    }
    //when i reach max lifeTime, stop aging
    private void OnMaxAge () {

        //Fire the spawn code on max age | Despawn object " Set lifestate to max
        switch (objectType) {
            case ObjectType.Fruit:
                if (spawnEvent != null) {
                    spawnEvent (SeedManagerScript.Instance.seedParent, SeedManagerScript.Instance.seedObjectPool, SeedManagerScript.Instance.seedObjectPool.GetChild (0).gameObject, gameObject);
                }

                lifeState = LifeState.ReachedMaxLifetime;
                FruitManagerScript.Instance.OnObjectPooling (gameObject);
                Debug.Log ($"{gameObject} stop aging | lifeTime: {lifeLifeTime} | lifeState: {lifeState}");

                break;
            case ObjectType.Seed:

                if (spawnEvent != null) {
                    spawnEvent (TreeManagerScript.Instance.treeActiveParent, TreeManagerScript.Instance.treeObjectPool, TreeManagerScript.Instance.treeObjectPool.GetChild (0).gameObject, gameObject);
                }
                lifeState = LifeState.ReachedMaxLifetime;
                SeedManagerScript.Instance.OnObjectPooling (gameObject);
                Debug.Log ($"{gameObject} stop aging | lifeTime: {lifeLifeTime} | lifeState: {lifeState}");

                break;
            default:
                break;
        }

    }

    //reset the values for when a seed is spawned again
    public void OnResetLifeState () {
        lifeState = LifeState.NotAging;
        lifeLifeTime = 0;
    }

    //coroutine to increase life after timeframe
    IEnumerator OnlifeLifetimeIncrease () {
        yield return new WaitForSeconds (Random.Range (time, 1));

        lifeLifeTime++;
        Debug.Log ($"{gameObject} lifeTime increasing | life lifeTime:{lifeLifeTime}");
        lifeState = LifeState.NotAging;

    }
}