using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CharacterControlsScript : MonoBehaviour {
    private Controls inputs;
    [SerializeField] CharacterWateringScript characterWateringScript;
    private CharacterInventoryScript characterInventoryScript;
    private CharacterInputScript characterInputScript;
    [SerializeField] GameObject seedSpawner;
    [SerializeField] UI_textDisplay uiScript;

    //asign input and get scripts
    void Start () {
        inputs = new Controls ();
        inputs.Enable ();
        inputs.CharacterContorls.Move.performed += OnMove;
        inputs.CharacterContorls.Interact.performed += OnInteract;
        inputs.CharacterContorls.Water.performed += OnWatering;
        inputs.CharacterContorls.Water.canceled += OnCancleWatering;
        inputs.CharacterContorls.Eat.performed += OnEating;
        inputs.CharacterContorls.Plant.performed += OnPlanting;

        characterInputScript = GetComponent<CharacterInputScript> ();
        characterInventoryScript = GetComponent<CharacterInventoryScript> ();

    }
    private void OnDestroy () {
        inputs.CharacterContorls.Move.performed -= OnMove;
        inputs.CharacterContorls.Interact.performed -= OnInteract;
        inputs.CharacterContorls.Water.performed -= OnWatering;
        inputs.CharacterContorls.Water.canceled -= OnCancleWatering;
        inputs.CharacterContorls.Eat.performed -= OnEating;
        inputs.CharacterContorls.Plant.performed -= OnPlanting;
    }
    // run actions when button pressed. The seed is planted. 

    private void OnPlanting (InputAction.CallbackContext obj) {
        if (CharacterManager.Instance.seedCount > 0) {

            if (CharacterManager.plantActions != null) {

                CharacterManager.plantActions (InteractionObjects.Seed);
                //run method that spawns items. Set spawn position to spawner in front of player.
                SeedManagerScript.Instance.seedObjectPool.GetChild (0).gameObject.GetComponent<SpawnScript> ().OnSpawnItem (SeedManagerScript.Instance.seedParent, SeedManagerScript.Instance.seedObjectPool, SeedManagerScript.Instance.seedObjectPool.GetChild (0).gameObject, seedSpawner);
            }

        }
    }

    //When pressing eating button, send off delegate actions
    private void OnEating (InputAction.CallbackContext obj) {
        if (CharacterManager.eatFruitAction != null) {
            CharacterManager.eatFruitAction (InteractionObjects.Fruit);
            Debug.Log("eating");
        }

    }
    //stip watering and increasing speed of tree growth
    private void OnCancleWatering (InputAction.CallbackContext obj) {
        characterWateringScript.OnCancleWatering ();

    }

    //when character waters the tree age increases.
    //it checks that the character has the watering can tool
    private void OnWatering (InputAction.CallbackContext obj) {

        if (characterWateringScript.OnCheckInventory ()) {
            characterWateringScript.OnWatering ();

        }

    }
    //when character interacts
    private void OnInteract (InputAction.CallbackContext obj) {
        switch (characterInputScript.OnCheckInput ()) {
            case InteractionObjects.Tool:
                Debug.Log ($"{gameObject} recieves {InteractionObjects.Tool}");
                //add watering can to inventory
                characterInventoryScript.OnAddToolToInventory ();
                break;
            case InteractionObjects.Fruit:
                Debug.Log ($"{gameObject} recieves {InteractionObjects.Fruit}");
                //add watering can to inventory
                GetComponent<CharacterInventoryScript> ().OnAddItemToCount (characterInputScript.OnCheckInput ());
                //despawn
                FruitManagerScript.Instance.OnObjectPooling (GetComponent<CharacterInputScript> ().OnSendItem ());
                break;
            case InteractionObjects.Seed:
                Debug.Log ($"{gameObject} recieves {InteractionObjects.Seed}");
                //add watering can to inventory
                GetComponent<CharacterInventoryScript> ().OnAddItemToCount (characterInputScript.OnCheckInput ());
                //despawn
                SeedManagerScript.Instance.OnObjectPooling (GetComponent<CharacterInputScript> ().OnSendItem ());
                break;
            default:
                break;
        }

    }

    //when character moves
    private void OnMove (InputAction.CallbackContext obj) {
        //send value to movement script
        GetComponent<CharacterMovementScript> ().OnMove (inputs.CharacterContorls.Move.ReadValue<Vector2> ());
    }

}