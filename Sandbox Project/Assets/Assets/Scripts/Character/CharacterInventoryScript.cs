using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInventoryScript : MonoBehaviour {
    public List<ToolScript> toolList = new List<ToolScript> ();
    int toolCount;
    [SerializeField] int fruitCount;

    [SerializeField] int seedCount;

    private void OnEnable () {
        CharacterManager.eatFruitAction += OnRemoveItemCount;
        CharacterManager.plantActions += OnRemoveItemCount;
    }

    private void OnDisable () {
        CharacterManager.eatFruitAction -= OnRemoveItemCount;
        CharacterManager.plantActions -= OnRemoveItemCount;
    }
    private void OnRemoveItemCount (InteractionObjects item) {
        switch (item) {
            case InteractionObjects.Fruit:
                if (fruitCount > 0) {
                    fruitCount--;
                    seedCount++;
                    CharacterManager.Instance.fruitCount = fruitCount;
                    CharacterManager.Instance.seedCount = seedCount;
                }
                break;
            case InteractionObjects.Seed:
                if (seedCount > 0) {
                    seedCount--;
                    CharacterManager.Instance.seedCount = seedCount;

                }
                break;
            default:
                break;
        }

    }

    public void OnAddToolToInventory () {
        //add tool script to list

        toolList.Add (GetComponent<CharacterInputScript> ().OnRecieveToolScript ());
        Debug.Log ($"Toolscript from the {toolList[toolCount].toolType} has been added to inventory");
        //change tools position when picked up to character's position. 
        toolList[toolCount].OnToolPickUp (transform);
        toolCount++;

    }
    //increase fruit count
    public void OnAddItemToCount (InteractionObjects item) {

        switch (item) {
            case InteractionObjects.Fruit:
                fruitCount++;
                CharacterManager.Instance.fruitCount = fruitCount;
                break;
            case InteractionObjects.Seed:
                seedCount++;
                CharacterManager.Instance.seedCount = seedCount;
                break;
            default:
                break;
        }

        if (CharacterManager.pickupActions != null) {
            Debug.Log ($"{gameObject} picks up {item} count increases");
            CharacterManager.pickupActions (item);
        }
    }

}