using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovementScript : MonoBehaviour {
    private Vector2 moveInput;
    private Rigidbody2D rb2D;
    [SerializeField] float speedMultiplier;
    void Start () {

    }
    //activate RB
    private void OnGetRigidBody () {
        rb2D = CharacterManager.Instance.OnGetRigidBody ();
    }
    //run methods
    private void FixedUpdate () {
        OnGetRigidBody ();
        OnMovement ();
    }
    //update velocity with value from input
    private void OnMovement () {
        rb2D.velocity = new Vector2 (moveInput.x * speedMultiplier * Time.deltaTime, rb2D.velocity.y);
        OnFlipCharacter ();

    }
    //get value from control script
    public void OnMove (Vector2 _moveInput) {
        moveInput = _moveInput;
    }
    //flip character in x axis based on move direction
    private void OnFlipCharacter () {
        if (rb2D.velocity.x < 0) {
            Vector2 tempVar = new Vector2 (-1, 1);
            transform.localScale = tempVar;

        } else if (rb2D.velocity.x > 0) {
            Vector2 tempVar = new Vector2 (1, 1);
            transform.localScale = tempVar;
        }
    }

}