using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;



public class CharacterManager : MonoBehaviour {

    public  delegate void PickupActions( InteractionObjects item);
    public static PickupActions pickupActions;
    public delegate void EatFruitAction(InteractionObjects item);
    public static EatFruitAction eatFruitAction;

    public delegate void PlantActions(InteractionObjects item);
    public static PlantActions plantActions;
    public delegate void PlantSeedActions(Transform parentTransform, Transform poolTransform, GameObject spawnObject, GameObject spawnerObject);
    public static PlantSeedActions plantSeedActions;
    
    public static CharacterManager Instance;
    
    [SerializeField] float characterHealth;

    
    private int _fruitCount;
    private int _seedCount;

    public int fruitCount{
        get{
            return _fruitCount;
        }
        set{
            if (value > _fruitCount || value < _fruitCount )
            {
                _fruitCount = value;
            }
        }
    }
    public int seedCount{
        get{
            return _seedCount;
        }
        set{
            if (value > _seedCount || value < _seedCount )
            {
                _seedCount = value;
            }
        }
    }

    private Rigidbody2D rb2D;

    private Controls inputs;

    [ContextMenu("check counts")]

    public void CheckCounts(){
        Debug.Log($"fruitcount is {fruitCount} seedcount is {seedCount}");
    }

    private void Awake () {
        Instance = this;

        rb2D = gameObject.GetComponent<Rigidbody2D> ();
        
    }
    public Rigidbody2D OnGetRigidBody () {
        return rb2D;
    }

    private void LateUpdate () {
       
    }
}