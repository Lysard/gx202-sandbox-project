using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterWateringScript : MonoBehaviour {
    private bool hasWateringCan = false;
    private bool isWatering = false;
    private GameObject tree;

    [SerializeField] CharacterInventoryScript characterInventoryScript;
    public enum WateringCanInput {
        HittingTree,
        HittingNothing
    }

    public WateringCanInput wateringCanInput = WateringCanInput.HittingNothing;

    //check watering can is in inventory
    public bool OnCheckInventory () {

        for (var i = 0; i < characterInventoryScript.toolList.Count; i++) {
            if (characterInventoryScript.toolList[i].toolType == ToolScript.ToolTypes.WateringCan) {
                hasWateringCan = true;
                Debug.Log ($"{this} checks if I have a watering can | Sets bool hasWateringCan to {hasWateringCan}");
            }

        }
        return hasWateringCan;
    }
    //sets bool
    //sets the watering object to active that is attached to the player
    //this object collides with the tree
    public void OnWatering () {
        isWatering = true;
        transform.GetChild (0).gameObject.SetActive (true);
        Debug.Log ($"Character isWatering: {isWatering}");
        OnWateringTree();
    }
    //It makes sure my watering object is touching the tree. 
    //speeds up tree ageing or slows it down
    private void OnWateringTree(){
        if (wateringCanInput == WateringCanInput.HittingTree)
        {
            if (tree != null)
            {
            tree.GetComponent<TreeLifeScript>().SpeedUpGrowing();
            Debug.Log($"{gameObject} is watering tree: {isWatering}");
                
            }
        }else
        {
            if (tree != null)
            {
                
            tree.GetComponent<TreeLifeScript>().SlowDownGrowing();
            }
        }
    }
    //stops speed ageing process
    //sets watering object to inactive
    //water bool false
    public void OnCancleWatering () {
        isWatering = false;
        transform.GetChild (0).gameObject.SetActive (false);
        Debug.Log ($"Character isWatering: {isWatering}");
        if (tree != null)
        {
        tree.GetComponent<TreeLifeScript>().SlowDownGrowing();
            
        }
    }
    //sets interaction state to nothing
    private void OnTriggerExit2D (Collider2D other) {
        if (hasWateringCan) {
            if (other.GetComponent<TreeLifeScript> () != null) {
                wateringCanInput = WateringCanInput.HittingNothing;
            }
        }

    }
    //sets interaction state to hitting tree when the water object hits the tree
    private void OnTriggerEnter2D (Collider2D other) {
        if (hasWateringCan) {
            if (other.GetComponent<TreeLifeScript> () != null) {
                tree = other.gameObject;
                wateringCanInput = WateringCanInput.HittingTree;
            }
        }

    }

    void Update () {

    }
}