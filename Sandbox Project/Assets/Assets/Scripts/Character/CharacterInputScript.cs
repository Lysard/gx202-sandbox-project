using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InteractionObjects {
    Tool,
    Tree,
    Seed,
    Water,
    Fruit,
    Nothing
}
public class CharacterInputScript : MonoBehaviour {

    private GameObject Tool;
    private GameObject Item;
    public InteractionObjects interactionObjects;
    private bool isTouchingSomething = false;
    void Start () {
        interactionObjects = InteractionObjects.Nothing;
    }
    private void OnTriggerEnter2D (Collider2D other) {

        // if I am colliding with a tool
        if (other.GetComponent<ToolScript> () != null) {
            ToolScript toolScript = other.GetComponent<ToolScript> ();
            //set the tool gameobject to be used
            Tool = other.gameObject;
            //set interaction state
            interactionObjects = InteractionObjects.Tool;
            Debug.Log ($"{gameObject} is Colliding with {toolScript.toolType} | Set interaction object to {interactionObjects}");

        } else if (other.GetComponent<GroundLifeTimeScript> () != null) {

            switch (other.GetComponent<GroundLifeTimeScript> ().objectType) {
                case ObjectType.Fruit:
                    //set interaction state
                    interactionObjects = InteractionObjects.Fruit;

                    break;
                case ObjectType.Seed:
                    //set interaction state
                    interactionObjects = InteractionObjects.Seed;

                    break;
                default:
                    break;
            }
            Debug.Log ($"{gameObject} is Colliding with {other.GetComponent<GroundLifeTimeScript>().objectType} | Set interaction object to {interactionObjects}");
            //set item game object to be used
            Item = other.gameObject;

        }

    }

    //send input info to other scripts
    public InteractionObjects OnCheckInput () {
        return interactionObjects;
    }
    //send out tool script to inventory
    public ToolScript OnRecieveToolScript () {

        return Tool.GetComponent<ToolScript> ();
    }
    //send item object to other scripts
    public GameObject OnSendItem () {
        return Item;
    }
    //set interaction to none when not touching
    private void OnTriggerExit2D (Collider2D other) {

        interactionObjects = InteractionObjects.Nothing;
    }

}