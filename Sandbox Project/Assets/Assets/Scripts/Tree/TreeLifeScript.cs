using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public enum TreeLifeStates {
    Unborn,
    Planted,
    Sapling,
    SmallTree,
    Tree,
    ReachedMaxLifetime,
    Dead

}
public class TreeLifeScript : MonoBehaviour {

    public TreeLifeStates treeLifeStates = TreeLifeStates.Unborn;

    [SerializeField] float age;
    [SerializeField] float growTime;
    [SerializeField] float maxGrowTime;
    [SerializeField] float minGrowTime;
    private bool isGrowing = false;
    private float maxAge;
    private void OnEnable () { }
    //script runs when tree is spawned from seed.
    public void OnPlanted () {
        maxAge = TreeManagerScript.Instance.SendMaxAgeFloat ();
        treeLifeStates = TreeLifeStates.Planted;
    }
    //changes life state based on age, also changes sprite of tree
    private void Update () {
        OnGrowing ();
        switch (age) {
            case 10:
                treeLifeStates = TreeLifeStates.Sapling;
                break;
            case 20:
                treeLifeStates = TreeLifeStates.SmallTree;
                gameObject.GetComponent<TreeSpriteManager> ().OnSetSmallTreeSprite ();
                break;
            case 30:
                treeLifeStates = TreeLifeStates.Tree;
                gameObject.GetComponent<TreeSpriteManager> ().OnSetTreeSprite ();
                break;
            default:
                break;
        }

    }

    public void OnResetLifeState () {
        treeLifeStates = TreeLifeStates.Unborn;
        age = 0;
        isGrowing = false;
    }

    //when the character waters the tree, the age time increases/decreases
    public void SpeedUpGrowing () {
        growTime = minGrowTime;
    }
    public void SlowDownGrowing () {
        growTime = maxGrowTime;
    }
    //age increases with coroutine, on max the tree starts dying and the fruit spawn
    private void OnGrowing () {
        if (treeLifeStates != TreeLifeStates.Unborn && treeLifeStates != TreeLifeStates.ReachedMaxLifetime) {
            if (age < maxAge && !isGrowing) {
                isGrowing = true;
                StartCoroutine (OnTreeGrowingTime ());
            } else if (age >= maxAge) {
                treeLifeStates = TreeLifeStates.ReachedMaxLifetime;
                Debug.Log ($"Tree has reached max age: {age} || Tree State: {treeLifeStates} || tree stopped aging");
                GetComponent<TreeFruitSpawnScrpt> ().OnFruitSpawn ();
                GetComponent<TreeDeathEvent> ().OnTreeDeathTimer ();

            }

        }

    }
    //check the life state
    public TreeLifeStates OnCheckTreeLifeStage () {
        return treeLifeStates;
    }
    //wait and increase age
    IEnumerator OnTreeGrowingTime () {
        yield return new WaitForSeconds (growTime);
        Debug.Log ($"Tree is growing | Current Age: {age}");
        age++;
        isGrowing = false;

    }
}