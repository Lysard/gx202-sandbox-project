
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TreeDeathEvent : MonoBehaviour
{
    [SerializeField] float deathTime;
    [SerializeField] float deathTimer;
    
   //check tree is max age, start counting up. if count reaches deathtime then reset tree state, drop fruits, reset tree sprites, send tree to object pool and reset timer
    public void OnTreeDeathTimer()
    {
        if (deathTimer >= deathTime)
            {
                GetComponent<TreeLifeScript>().OnResetLifeState();
                GetComponent<TreeFruitSpawnScrpt>().OnFruitDrop();
                GetComponent<TreeSpriteManager>().OnResetSprite();
                TreeManagerScript.Instance.OnObjectPool(gameObject);
                deathTimer = 0;
            }
        if (gameObject.GetComponent<TreeLifeScript>().OnCheckTreeLifeStage() == TreeLifeStates.ReachedMaxLifetime)
        {
            StartCoroutine(OnDeathCountDown());
        }
          
    }

    IEnumerator OnDeathCountDown(){
        yield return new WaitForSeconds(Random.Range(0.5f,1));
        if (deathTimer <= deathTime)
        {
        deathTimer ++;
            OnTreeDeathTimer();
        }
         
    }
}
