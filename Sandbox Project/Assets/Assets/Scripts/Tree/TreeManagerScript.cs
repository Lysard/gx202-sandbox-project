using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeManagerScript : MonoBehaviour {
    public static TreeManagerScript Instance;
    public Transform treeActiveParent;
    public Transform treeObjectPool;
    private float maxWaterLevel;
    [SerializeField] float maxAge = 10;
    public delegate void TreeDeathAction ();

    public static TreeDeathAction treeDeathEvent;
    //singleton
    private void Awake () {
        Instance = this;
    }
    //send age data
    public float SendMaxAgeFloat () {
        return maxAge;
    }
    //place tree in object pool
    public void OnObjectPool (GameObject tree) {
        tree.GetComponent<TreeLifeScript> ().OnResetLifeState ();
        tree.transform.parent = treeObjectPool;
    }

}