using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeSpriteManager : MonoBehaviour
{
    [SerializeField] List<Sprite> treeSpriteList = new List<Sprite>();

    private void Awake() {
        transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = treeSpriteList[0];
    }
    public void OnSetSmallTreeSprite(){
        transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = treeSpriteList[1];
    }
    public void OnSetTreeSprite(){
        transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = treeSpriteList[2];
    }

    public void OnResetSprite(){
        transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = treeSpriteList[0];
    }
}
