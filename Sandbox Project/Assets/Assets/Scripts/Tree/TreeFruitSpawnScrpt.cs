using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeFruitSpawnScrpt : MonoBehaviour {
    private GameObject fruit;

    public List<GameObject> spawnedFruitList = new List<GameObject> ();
    
    [SerializeField] int spawnAmount;
    
    [SerializeField] Transform spawnTransform;
    private GameObject droppedFruit;
    [SerializeField] float maxFruit = 4;
    private void OnEnable () {
        TreeManagerScript.treeDeathEvent += OnFruitSpawn;
    }
    private void OnDisable () {
        TreeManagerScript.treeDeathEvent -= OnFruitSpawn;
    }

    //take fruit from object pool to parent. Spawn fruit at spawn point. freeze rigidbody. put fruit that was spawned into a list. 
    //Increase spawn counter(spawnAmount) and run coroutine to restart method
    public void OnFruitSpawn () {

        if (FruitManagerScript.Instance.OnSendObjectPoolTransformPool ().childCount < 2) {
            Instantiate (FruitManagerScript.Instance.OnSendObjectPoolTransformPool ().GetChild (0), FruitManagerScript.Instance.OnSendObjectPoolTransformPool ());
        }
        if (GetComponent<TreeLifeScript> ().OnCheckTreeLifeStage () == TreeLifeStates.ReachedMaxLifetime) {

            fruit = FruitManagerScript.Instance.OnSendObjectPoolTransformPool ().GetChild (0).gameObject;
            fruit.transform.parent = FruitManagerScript.Instance.OnSendObjectPoolTransformParent ();

            fruit.transform.position = new Vector3 (spawnTransform.GetChild (spawnAmount).transform.position.x, spawnTransform.GetChild (spawnAmount).transform.position.y);

            spawnedFruitList.Add (fruit);
            spawnAmount++;
            fruit.GetComponent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.FreezeAll;
            StartCoroutine (OnSpawnWait ());
        }

    }
    //Checks if the tree is done spawning - run from another script
    public bool DoneSpawning () {
        bool isDoneSpawning;
        if (spawnedFruitList.Count >= maxFruit) {
            isDoneSpawning = true;
        } else {
            isDoneSpawning = false;
        }
        return isDoneSpawning;
    }

    //unfreeze the fruits in the list and remove them from the list
    //set the position again of the spawned fruits
    //remove fruits from list and reset spawnAmount (spawn counter)
    public void OnFruitDrop () {

        if (spawnedFruitList.Count >= maxFruit) {
            for (var i = 0; i < spawnedFruitList.Count; i++) {
                Debug.Log (i);
                droppedFruit = spawnedFruitList[i];
                droppedFruit.GetComponent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.None;
                droppedFruit.GetComponent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.FreezeRotation | RigidbodyConstraints2D.FreezePositionX;
                droppedFruit.GetComponent<Rigidbody2D> ().position = new Vector3 (spawnTransform.GetChild (i).transform.position.x, spawnTransform.GetChild (i).transform.position.y);
                Debug.Log ($"{spawnedFruitList[i]} constraint is lifted");
            }

            spawnedFruitList.RemoveRange (0, spawnedFruitList.Count);
            spawnAmount = 0;

        }

    }
    //coroutine to rerun method
    IEnumerator OnSpawnWait () {
        yield return new WaitForSeconds (Random.Range (1, 3));
        if (spawnAmount < spawnTransform.childCount) {
            OnFruitSpawn ();

        }

    }

}