using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolScript : MonoBehaviour
{
   public enum ToolTypes
    {
        WateringCan,
        Cutter
    }
    public ToolTypes toolType;
    private float currentWaterLevel;

    

    
    public void OnToolPickUp(Transform characterTransform){
        
        //disable tool sprite
        ToolManager.Instance.OnObjectPooling(gameObject);
        if (CharacterManager.pickupActions != null)
        {
            Debug.Log($"{gameObject} picks up tool");
            CharacterManager.pickupActions(InteractionObjects.Tool);
        }
        
       
    }
}
