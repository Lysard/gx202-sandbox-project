using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolManager : MonoBehaviour
{
    public static ToolManager Instance;
    [SerializeField] Transform activeToolParent;
    [SerializeField] Transform toolObjectPool;

    private void Awake() {
        Instance = this;
    }
    void Start()
    {
        
    }

    // Update is called once per frame
   public void OnObjectPooling(GameObject _tool){
       _tool.transform.parent = toolObjectPool;
   }
}
