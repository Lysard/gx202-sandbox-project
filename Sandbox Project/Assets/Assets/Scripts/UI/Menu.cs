using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
   [SerializeField] GameObject menu;
   [SerializeField] GameObject reference;

   public void OnPause(){
    menu.SetActive(true);
   }

   public void UnPause(){
    menu.SetActive(false);
   }
   public void OnReference(){
      reference.SetActive(true);
   }
   public void OnBack(){
      reference.SetActive(false);
   }
   public void Quit(){
    Application.Quit();
   }
   public void LoadGame(){
    SceneManager.LoadScene(1);
   }
   public void LoadMenu(){
    SceneManager.LoadScene(0);

   }
}
