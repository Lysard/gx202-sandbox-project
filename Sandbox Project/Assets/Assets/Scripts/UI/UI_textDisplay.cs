using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_textDisplay : MonoBehaviour {
    [SerializeField] TextMeshProUGUI fruitCount;
    [SerializeField] TextMeshProUGUI seedCount;
    [SerializeField] ParticleSystem fruitPS;
    [SerializeField] ParticleSystem seedPS;

    private void OnEnable () {
        CharacterManager.pickupActions += OnUpdateUINumber;
        CharacterManager.pickupActions += OnParticlePlay;

        CharacterManager.eatFruitAction += OnUpdateUINumber;
        CharacterManager.eatFruitAction += OnParticlePlay;
        CharacterManager.plantActions += OnUpdateUINumber;
    }

    private void OnDisable () {
        CharacterManager.pickupActions -= OnUpdateUINumber;
        CharacterManager.pickupActions -= OnParticlePlay;

        CharacterManager.eatFruitAction -= OnUpdateUINumber;
        CharacterManager.eatFruitAction -= OnParticlePlay;
        CharacterManager.plantActions -= OnUpdateUINumber;
    }
    //play particle when picking up object
    private void OnParticlePlay (InteractionObjects objectType) {
        switch (objectType) {
            case InteractionObjects.Fruit:
                fruitPS.Play ();
                break;
            case InteractionObjects.Seed:
                seedPS.Play ();
                break;
            default:
                break;
        }

    }

    //update the count of the object, based on what object it is Seed/Fruit
    public void OnUpdateUINumber (InteractionObjects objectType) {

        StartCoroutine (OnWaitUpdateNumbers (objectType));
    }

    IEnumerator OnWaitUpdateNumbers (InteractionObjects objectType) {
        yield return new WaitForSeconds (0.1f);
        switch (objectType) {
            case InteractionObjects.Fruit:
                fruitCount.text = CharacterManager.Instance.fruitCount.ToString ();
                //to update seed count when eating fruit

                seedCount.text = CharacterManager.Instance.seedCount.ToString ();
                OnParticlePlay (InteractionObjects.Seed);

                Debug.Log ($"FruitCount updates to {fruitCount}");
                break;
            case InteractionObjects.Seed:
                seedCount.text = CharacterManager.Instance.seedCount.ToString ();
                Debug.Log ($"FruitCount updates to {fruitCount}");
                break;
            default:
                break;
        }
    }

}