using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitManagerScript : MonoBehaviour {
    public static FruitManagerScript Instance;

    [SerializeField] Transform fruitParent;
    [SerializeField] Transform fruitObjectPool;
    [SerializeField] float maxLifeTime;

    //Singleton
    private void Awake () {
        Instance = this;
    }
    //send object life data
    public float OnSendMaxLifeFloat () {
        return maxLifeTime;
    }
    //Send parent object to other script
    public Transform OnSendObjectPoolTransformParent () {
        return fruitParent;
    }
    //send pool object to other script
    public Transform OnSendObjectPoolTransformPool () {
        return fruitObjectPool;
    }
    //send object to inactive pool
    public void OnObjectPooling (GameObject fruit) {
        fruit.GetComponent<GroundLifeTimeScript> ().OnResetLifeState ();
        fruit.transform.parent = fruitObjectPool;
    }
}