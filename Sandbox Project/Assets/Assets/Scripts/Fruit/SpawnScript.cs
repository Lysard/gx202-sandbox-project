using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnScript : MonoBehaviour
{
    private void OnEnable() {
        GroundLifeTimeScript.spawnEvent += OnSpawnItem;
        CharacterManager.plantSeedActions += OnSpawnItem;
    }

    private void OnDisable() {
        GroundLifeTimeScript.spawnEvent -= OnSpawnItem;
        CharacterManager.plantSeedActions -= OnSpawnItem;
    }
    // check pool for children, put object being spawned in parent, put object being spawned on the spawner object's position
    public void OnSpawnItem(Transform parentTransform, Transform poolTransform, GameObject spawnObject, GameObject spawnerObject)
    {
        if (poolTransform.childCount< 2)
        {
            Instantiate(poolTransform.GetChild(0),poolTransform);
        }
            
       
        spawnObject.transform.parent = parentTransform;
        spawnObject.GetComponent<Rigidbody2D>().position = spawnerObject.transform.position;
        Debug.Log($"spawning {spawnObject} to position {spawnerObject}");
        //if it's a tree, plant
        if (spawnObject.GetComponent<TreeLifeScript>() != null)
        {
            spawnObject.GetComponent<TreeLifeScript>().OnPlanted();
        }
    }
}
