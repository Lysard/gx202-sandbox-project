using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeedManagerScript : MonoBehaviour {
    public static SeedManagerScript Instance;
    public Transform seedParent;
    public Transform seedObjectPool;

    [SerializeField] float maxLifeTime = 30;

    //Singleton
    private void Awake () {
        Instance = this;
    }
    //send object life data
    public float OnSendMaxLifeFloat () {
        return maxLifeTime;
    }

    //send object to inactive pool
    public void OnObjectPooling (GameObject seed) {
        seed.GetComponent<GroundLifeTimeScript> ().OnResetLifeState ();
        seed.transform.parent = seedObjectPool;
    }
}